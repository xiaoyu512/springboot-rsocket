package com.hsy.spring.boot.rsocket.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 请求入参
 *
 * @author HuoShengyu
 * @version 1.0
 * @date 2019-11-28 14:46:07
 */
@Getter
@Setter
@Builder
@ToString
public class RSocketReq {
	private String name;
	private String message;
}
