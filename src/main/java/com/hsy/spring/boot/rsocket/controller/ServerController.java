package com.hsy.spring.boot.rsocket.controller;

import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import com.hsy.spring.boot.rsocket.domain.RSocketReq;
import com.hsy.spring.boot.rsocket.service.RSocketService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * RSocket服务端
 *
 * @author HuoShengyu
 * @version 1.0
 * @date 2019-11-28 14:45:34
 */
@Controller
public class ServerController {
	private final RSocketService rsocketService;

	public ServerController(RSocketService rsocketService) {
		this.rsocketService = rsocketService;
	}

	@MessageMapping("add")
	public Mono<Void> add(RSocketReq rsocketReq) {
		rsocketService.add(rsocketReq);
		return Mono.empty();
	}

	@MessageMapping("getOne")
	public Mono<RSocketReq> getOne(RSocketReq rsocketReq) {
		return rsocketService.getOne(rsocketReq);
	}

	@MessageMapping("getAll")
	public Flux<RSocketReq> getAll(RSocketReq rsocketReq) {
		return rsocketService.getAll(rsocketReq);
	}

	@MessageExceptionHandler
	public Mono<RSocketReq> handleException(Exception e) {
		return Mono.just(RSocketReq.builder().message(e.getMessage()).build());
	}
}
