package com.hsy.spring.boot.rsocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 *
 * @author HuoShengyu
 * @version 1.0
 * @date 2019-10-18 13:00:00
 */
@SpringBootApplication
public class RSocketRun {
	public static void main(String[] args) {
		SpringApplication.run(RSocketRun.class, args);
	}
}
