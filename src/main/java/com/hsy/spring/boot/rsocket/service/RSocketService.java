package com.hsy.spring.boot.rsocket.service;

import com.hsy.spring.boot.rsocket.domain.RSocketReq;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * 服務接口
 *
 * @author HuoShengyu
 * @version 1.0
 * @date 2019-11-28 14:46:25
 */
public interface RSocketService {
	/**
	 * 
	 * @param rsocketReq
	 */
	void add(RSocketReq rsocketReq);

	/**
	 * 
	 * @param rsocketReq
	 * @return
	 */
	Mono<RSocketReq> getOne(RSocketReq rsocketReq);

	/**
	 * 
	 * @param rsocketReq
	 * @return
	 */
	Flux<RSocketReq> getAll(RSocketReq rsocketReq);
}
