package com.hsy.spring.boot.rsocket.controller;

import org.reactivestreams.Publisher;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsy.spring.boot.rsocket.domain.RSocketReq;

/**
 * RSocket客户端
 *
 * @author HuoShengyu
 * @version 1.0
 * @date 2019-11-28 14:45:17
 */
@Lazy
@RestController
@RequestMapping("/rsocket")
public class ClientController {
	private final RSocketRequester rsocketRequester;

	public ClientController(RSocketRequester rsocketRequester) {
		this.rsocketRequester = rsocketRequester;
	}

	@GetMapping(value = "/add")
	public Publisher<Void> add() {
		return rsocketRequester.route("add").data(RSocketReq.builder().name("hello").build()).send();
	}

	@GetMapping(value = "/getOne/{data}")
	public Publisher<RSocketReq> getOne(@PathVariable("data") String data) {
		return rsocketRequester.route("getOne").data(RSocketReq.builder().name(data).build()).retrieveMono(RSocketReq.class);
	}

	@GetMapping(value = "/getAll/{data}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Publisher<RSocketReq> getAll(@PathVariable("data") String data) {
		return rsocketRequester.route("getAll").data(RSocketReq.builder().name(data).build()).retrieveFlux(RSocketReq.class);
	}
}
