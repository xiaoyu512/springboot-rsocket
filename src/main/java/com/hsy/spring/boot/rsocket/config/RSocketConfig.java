package com.hsy.spring.boot.rsocket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.rsocket.RSocketRequester;

/**
 * 客户端配置
 *
 * @author HuoShengyu
 * @version 1.0
 * @date 2019-11-28 14:48:25
 */
@Lazy
@Configuration
public class RSocketConfig {
	@Bean
	RSocketRequester rsocketRequester(RSocketRequester.Builder rsocketRequesterBuilder) {
		return rsocketRequesterBuilder.connectTcp("127.0.0.1", 31000).block();
	}
}
